#!/bin/bash

TOP_DIR=$(cd $(dirname "$0") && pwd)
if [[ -f /etc/nativi.conf ]];then
	source /etc/nativi.conf
else
	echo "Can't find /etc/nativi.conf"
	exit 1
fi

ARGS=""

myrun(){
	echo "$1"
	$1
}

echo "**************"
echo "nativi setup"
echo "**************"

# Must have a global scope ipv6 address
if [[ $(ip -6 addr show scope global | grep inet6 | wc -l) == 0 ]]; then
	echo "No global scope ipv6 address!"
	exit 1
fi
# Check arguments
if [ -z "$SRC_PREFIX_ADDR" ] || [ -z "$SRC_PREFIX_LEN" ];then
	echo "Error:no src ipv6 address!"
	exit 1
fi
if [ $SRC_PREFIX_LEN -gt 128 ];then
	echo "Error:source ipv6 length is greater than 128!"
	exit 1
fi
if [ -z "$DST_PREFIX_ADDR" ] || [ -z "$DST_PREFIX_LEN" ];then
	echo "Error:no dst ipv6 address!"
	exit 1
fi
if [ $DST_PREFIX_LEN -gt 128 ];then
	echo "Error:dst ipv6 length is greater than 128!"
	exit 1
fi

echo "src_prefix:${SRC_PREFIX_ADDR}/${SRC_PREFIX_LEN}"
echo "dst_prefix:${DST_PREFIX_ADDR}/${DST_PREFIX_LEN}"
echo

ARGS="nativi_src_prefix_addr=$SRC_PREFIX_ADDR nativi_src_prefix_len=$SRC_PREFIX_LEN \
nativi_dst_prefix_addr=$DST_PREFIX_ADDR nativi_dst_prefix_len=$DST_PREFIX_LEN "


echo
#set -ex

# Load the nf_nativi module
myrun "modprobe -r nf_nativi"
myrun "modprobe nf_nativi $ARGS"

# Enable the nativi network interface
myrun "ifconfig nativi mtu 1500 up"

# Install the route to the nativi prefix
myrun "ip -6 route add ${SRC_PREFIX_ADDR}/${SRC_PREFIX_LEN} dev nativi"

# Enable ipv6 and ipv4 forwarding
myrun "sysctl -w net.ipv4.conf.all.forwarding=1"
myrun "sysctl -w net.ipv6.conf.all.forwarding=1"

exit 0
