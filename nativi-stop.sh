#!/bin/bash

if [[ $(lsmod |grep nativi |wc -l) -gt 0 ]];then
	modprobe -r nf_nativi
	if [[ $? -ne 0 ]];then
		echo "Can't unload module nf_nativi!"
		exit 1
	else
		echo "Unload module nf_nativi sucessfully!"
		exit 0
	fi
else
	echo "Module nf_nativi doesn't load!"
	exit 0
fi
	
