/*
 * NAT64 - Network Address Translator IPv6 to IPv4
 *
 * Copyright (C) 2010 Viagenie Inc. http://www.viagenie.ca
 *
 * Authors:
 *      Jean-Philippe Dionne <jean-philippe.dionne@viagenie.ca>
 *      Simon Perreault <simon.perreault@viagenie.ca>
 *      Marc Blanchet <marc.blanchet@viagenie.ca>
 *
 * NAT64 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NAT64 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with NAT64.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>	/* Needed by all modules */
#include <linux/moduleparam.h>  /* Needed for module_param */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/init.h>		/* Needed for the macros */

#include <linux/inet.h>
#include <linux/in6.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/icmp.h>
#include <linux/icmpv6.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netfilter/nf_conntrack_common.h>
#include <linux/if_arp.h>
#include <linux/bitmap.h>
#include <net/ipv6.h>
#include <linux/inetdevice.h>

#include <net/ip.h>
#include <net/route.h>
#include <net/ip6_route.h>
#include <net/netfilter/ipv4/nf_conntrack_ipv4.h>

#include "nf_nat64_config.h"
//#include "nf_nativi_session.h"
#include "nf_nat64_ip6_frag.c"
#include "nf_nat64_ip6_reasm.c"

#define NAT64_NETDEV_NAME "nativi"
#define NAT64_VERSION "0.1"

/* XXX Missing defines in kernel headers */
#define ICMP_MINLEN 8
#define ICMP_ROUTERADVERT       9
#define ICMP_ROUTERSOLICIT      10
#define ICMP_INFOTYPE(type) \
	((type) == ICMP_ECHOREPLY || (type) == ICMP_ECHO || \
	 (type) == ICMP_ROUTERADVERT || (type) == ICMP_ROUTERSOLICIT || \
	 (type) == ICMP_TIMESTAMP || (type) == ICMP_TIMESTAMPREPLY || \
	 (type) == ICMP_INFO_REQUEST || (type) == ICMP_INFO_REPLY || \
	 (type) == ICMP_ADDRESS || (type) == ICMP_ADDRESSREPLY)
#define IP_DEFRAG_NAT64 1234

/* TODO Set this parameter through netlink */
static int nativi_src_prefix_len = 96;
static char *nativi_src_prefix_addr = "0064:FF9B::";
static int nativi_dst_prefix_len = 96;
static char *nativi_dst_prefix_addr = "0064:FF9B::";
//static char *nativi_ipv4_addr = NULL;
//static int nativi_ipv4_mask = 32;

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jean-Philippe Dionne <jpdionne@viagenie.ca>");
MODULE_DESCRIPTION("nf_nativi");

module_param(nativi_src_prefix_addr, charp, 0000);
MODULE_PARM_DESC(nativi_src_prefix_addr, "nativi src prefix (default: 0064:FF9B::)");
module_param(nativi_src_prefix_len, int, 0000);
MODULE_PARM_DESC(nativi_src_prefix_len, "nativi src prefix len (default: 96)");
module_param(nativi_dst_prefix_addr, charp, 0000);
MODULE_PARM_DESC(nativi_dst_prefix_addr, "nativi dst prefix (default: 0064:FF9B::)");
module_param(nativi_dst_prefix_len, int, 0000);
MODULE_PARM_DESC(nativi_dst_prefix_len, "nativi dst prefix len (default: 96)");
//module_param(nativi_ipv4_addr, charp, 0000);
//MODULE_PARM_DESC(nativi_ipv4_addr, "nativi ipv4 address (must be set manually)");
//module_param(nativi_ipv4_mask, int, 0000);
//MODULE_PARM_DESC(nativi_ipv4_mask, "nativi ipv4 mask (default: 32)");


struct nativi_struct
{
        struct net_device *dev;
};

struct net_device *nativi_dev;

/* add necessary functions */
static inline int ip_skb_dst_mtu(struct sk_buff *skb)
{
	struct inet_sock *inet = skb->sk ? inet_sk(skb->sk) : NULL;
	return (inet && inet->pmtudisc == IP_PMTUDISC_PROBE) ?
		skb_dst(skb)->dev->mtu : dst_mtu(skb_dst(skb));
}

/**
 * \return	The payload length of the translated packet.
 */
static int
nativi_input_ipv6_recur(int recur, struct ipv6hdr *ip6, int len)
{
	struct ipv6_opt_hdr     *ip6e;
	struct udphdr		*uh;
	struct tcphdr		*th;
	struct icmp6hdr		*icmp6;
	uint16_t		 hlen;
	uint8_t			 proto;
	/* struct frag_hdr		*frag_header; */

	hlen  = sizeof(*ip6);
	proto = ip6->nexthdr;

next_header:
	switch (proto) {

	case IPPROTO_HOPOPTS:
	case IPPROTO_ROUTING:
	case IPPROTO_DSTOPTS:
		ip6e = (struct ipv6_opt_hdr*)((char *)ip6 + hlen);

		if ((len -= sizeof(*ip6e)) < 0)
			return 0;

		proto = ip6e->nexthdr;
		hlen += ip6e->hdrlen * 8;

		goto next_header;

	case IPPROTO_FRAGMENT:
		/* The kernel should be handling fragments for us. DROP. */
        WARN_ON_ONCE(1);
        return 0;
		
		/* frag_header = (struct frag_hdr *)((char *)ip6 + hlen); */

		/* if ((len -= sizeof(struct frag_hdr)) < 0) */
			/* return 0; */

		/* proto = frag_header->nexthdr; */
		/* hlen += 8; */

        /* if is not first fragment return len */
		/* if (frag_header->frag_off & htons(0xfff8)) { */
			/* return len; */
		/* } else { */
			/* goto next_header; */
		/* } */

	case IPPROTO_UDP:
		uh = (struct udphdr *)((char *)ip6 + hlen);

		if ((len -= sizeof(*uh)) < 0)
			return 0;

		return len + sizeof(*uh);

	case IPPROTO_TCP:
		th = (struct tcphdr *)((char *)ip6 + hlen);

		if ((len -= sizeof(*th)) < 0)
			return 0;

		return len + sizeof(*th);

	case IPPROTO_ICMPV6:
		icmp6 = (struct icmp6hdr *)((char *)ip6 + hlen);

		if ((len -= sizeof(*icmp6)) < 0)
			return 0;

		if (icmp6->icmp6_type & ICMPV6_INFOMSG_MASK) {
			return len + 8;
		} else {
			int size = nativi_input_ipv6_recur(recur + 1,
					(struct ipv6hdr *)(icmp6 + 1), len);
			return size ? size + sizeof(struct iphdr) + 8 : 0;
		}

	default:
		/* Ignore other protocols. */
		return 0;
	}

	/* Should never get here. */
	WARN_ON_ONCE(1);
	return 0;
}

static struct sk_buff*
nativi_alloc_skb(int tlen, int paylen)
{
        struct sk_buff *skb;
        skb = alloc_skb(LL_MAX_HEADER + tlen + paylen, GFP_ATOMIC);

        if (!skb) {
                return NULL;
        }

        skb_reserve(skb, LL_MAX_HEADER);
        skb_reset_mac_header(skb);
        skb_reset_network_header(skb);

        skb_set_transport_header(skb, tlen);

        skb_put(skb, tlen + paylen);

        return skb;
}

inline void *
ip_data(struct iphdr *ip4)
{
	return (char *)ip4 + ip4->ihl*4;
}


static void
checksum_adjust(uint16_t *sum, uint16_t old, uint16_t new, int udp)
{
        uint32_t s;

        if (udp && !*sum)
                return;

        s = *sum + old - new;
        *sum = (s & 0xffff) + (s >> 16);

        if (udp && !*sum)
                *sum = 0xffff;
}

static void
checksum_remove(uint16_t *sum, uint16_t *begin, uint16_t *end, int udp)
{
        while (begin < end)
                checksum_adjust(sum, *begin++, 0, udp);
}

static void
checksum_add(uint16_t *sum, uint16_t *begin, uint16_t *end, int udp)
{
        while (begin < end)
                checksum_adjust(sum, 0, *begin++, udp);
}


static void
adjust_checksum_ipv6_to_ipv4(uint16_t *sum, struct ipv6hdr *ip6,
		struct iphdr *ip4, int udp)
{
	WARN_ON_ONCE(udp && !*sum);

	checksum_remove(sum, (uint16_t *)&ip6->saddr,
			(uint16_t *)(&ip6->saddr + 2), udp);

	checksum_add(sum, (uint16_t *)&ip4->saddr,
			(uint16_t *)(&ip4->saddr + 2), udp);
}

static void
adjust_checksum_ipv4_to_ipv6(uint16_t *sum, struct iphdr *ip4,
		struct ipv6hdr *ip6, int udp)
{
	WARN_ON_ONCE(udp && !*sum);

	checksum_remove(sum, (uint16_t *)&ip4->saddr,
			(uint16_t *)(&ip4->saddr + 2), udp);

	checksum_add(sum, (uint16_t *)&ip6->saddr,
			(uint16_t *)(&ip6->saddr + 2), udp);
}


static void
checksum_change(uint16_t *sum, uint16_t *x, uint16_t new, int udp)
{
        checksum_adjust(sum, *x, new, udp);
        *x = new;
}

static struct iphdr *
nativi_xlate_ipv6_to_ipv4(struct ipv6hdr *ip6, struct iphdr *ip4, int plen,
		int recur)
{

	struct ipv6_opt_hdr *ip6e;
	struct udphdr *uh;
	struct tcphdr *th;
	struct icmphdr *ih;
	struct icmp6hdr *icmp6;
	uint16_t frag_flag = 0;

	ip4->version = 4;
	ip4->ihl = 5;
	ip4->tos = ip6->priority;
	ip4->tot_len = htons(sizeof(*ip4) + plen);
	ip4->id = 0;
	/* ip4->frag_off = htons(IP_DF); */
    ip4->frag_off = 0;
	ip4->ttl = ip6->hop_limit - 1;
	ip4->protocol = ip6->nexthdr;

	/* Skip extension headers. */
	ip6e = (struct ipv6_opt_hdr *)(ip6 + 1);

	while (ip4->protocol == IPPROTO_HOPOPTS ||
		ip4->protocol == IPPROTO_ROUTING ||
		ip4->protocol == IPPROTO_DSTOPTS)
	{
			ip4->protocol = ip6e->nexthdr;
			ip6e = (struct ipv6_opt_hdr *)((char *)ip6e + ip6e->hdrlen * 8);
	}

		*(struct in_addr*)&ip4->daddr = nativi_extract(&ip6->daddr, 0);
		*(struct in_addr*)&ip4->saddr = nativi_extract(&ip6->saddr, 1);

	switch (ip4->protocol) {
	case IPPROTO_UDP:
		uh = ip_data(ip4);
		memcpy(uh, ip6e, plen);
//		checksum_change(&uh->check, recur % 2 ? &uh->dest :
//				&uh->source, s->s_binding->b_sport4, 1);
		adjust_checksum_ipv6_to_ipv4(&uh->check, ip6, ip4, 1);
		break;
	case IPPROTO_TCP:
		th = ip_data(ip4);
		memcpy(th, ip6e, plen);
//		checksum_change(&th->check, recur % 2 ? &th->dest :
//				&th->source, s->s_binding->b_sport4, 0);
		adjust_checksum_ipv6_to_ipv4(&th->check, ip6, ip4, 0);
		break;
	case IPPROTO_ICMPV6:
		ih = ip_data(ip4);
		memcpy(ih, ip6e, plen);
		/* lynx */
		if (frag_flag) {
			ip4->protocol = IPPROTO_ICMP;
			break;
		}
		if (ih->type & ICMPV6_INFOMSG_MASK) {
			switch (ih->type) {
			case ICMPV6_ECHO_REQUEST:
				ih->type = ICMP_ECHO;
				break;
			case ICMPV6_ECHO_REPLY:
				ih->type = ICMP_ECHOREPLY;
				break;
			default:
				return NULL;
			}
		} else {
			switch (ih->type) {
			case ICMPV6_DEST_UNREACH:
				ih->type = ICMP_DEST_UNREACH;
				switch (ih->code) {
				case ICMPV6_NOROUTE:
				case ICMPV6_NOT_NEIGHBOUR:
				case ICMPV6_ADDR_UNREACH:
					ih->code = ICMP_HOST_UNREACH;
					break;
				case ICMPV6_ADM_PROHIBITED:
					ih->code = ICMP_HOST_ANO;
					break;
				case ICMPV6_PORT_UNREACH:
					ih->code = ICMP_PORT_UNREACH;
					break;
				default:
					return NULL;
				}
				break;
			case ICMPV6_PKT_TOOBIG:
				icmp6 = (struct icmp6hdr *)(ip6+1);
				ih->type = ICMP_DEST_UNREACH;
				ih->code = ICMP_FRAG_NEEDED;
				if (ntohl(icmp6->icmp6_mtu) >= 0xffff) {
					ih->un.frag.mtu = 0xffff;
				} else {
					ih->un.frag.mtu = htons(ntohl(icmp6->icmp6_mtu)-20);
				}
				break;
			case ICMPV6_TIME_EXCEED:
				ih->type = ICMP_TIME_EXCEEDED;
				break;
			case ICMPV6_PARAMPROB:
				if (ih->code == ICMPV6_UNK_NEXTHDR)
				{
					ih->type = ICMP_DEST_UNREACH;
					ih->code = ICMP_PROT_UNREACH;
				} else {
					ih->type = ICMP_PARAMETERPROB;
					ih->code = 0;
				}
				/* TODO update pointer */
				break;
			default:
				return NULL;
			}
			nativi_xlate_ipv6_to_ipv4(
				(struct ipv6hdr *)((char *)ip6e + 8),
				(struct iphdr *)(ih + 1),
				plen - ((char *)ip6e + 8 - (char *)ip6),
					recur + 1);

		}
		ih->checksum = 0;
		ih->checksum = ip_compute_csum(ih, plen);
		ip4->protocol = IPPROTO_ICMP;
		break;
	default:
		WARN_ON_ONCE(1);
	}

	/* Compute the ip header checksum */
	ip4->check = 0;
	ip4->check = ip_fast_csum(ip4, ip4->ihl);

	return ip4;
}

static int
nativi_output_ipv4_2(struct sk_buff *skb)
{
	struct iphdr *iph = ip_hdr(skb);
	struct flowi fl;
	struct rtable *rt;

	skb->protocol = htons(ETH_P_IP);

	memset(&fl, 0, sizeof(fl));
	fl.fl4_dst = iph->daddr;
	fl.fl4_tos = RT_TOS(iph->tos);
	fl.proto = skb->protocol;
	if (ip_route_output_key(&init_net, &rt, &fl))
	{
		printk("nf_nativi: ip_route_output_key failed\n");
		return -1;
	}

	if (!rt)
	{
		printk("nf_nativi: rt null\n");
		return -1;
	}

	skb->dev = rt->u.dst.dev;
	skb_dst_set(skb, (struct dst_entry *)rt);
		
	if(ip_local_out(skb)) {
	       printk("nf_nativi: ip_local_out failed\n");
	       return -1;
	}
		
	return 0;
}

static int
nativi_output_ipv4(struct sk_buff *skb)
{
	struct iphdr *iph = ip_hdr(skb);
	struct flowi fl;
	struct rtable *rt;

	skb->protocol = htons(ETH_P_IP);

	memset(&fl, 0, sizeof(fl));
	fl.fl4_dst = iph->daddr;
	fl.fl4_tos = RT_TOS(iph->tos);
	fl.proto = skb->protocol;
	if (ip_route_output_key(&init_net, &rt, &fl))
	{
		printk("nf_nativi: ip_route_output_key failed\n");
		return -1;
	}

	if (!rt)
	{
		printk("nf_nativi: rt null\n");
		return -1;
	}

	skb->dev = rt->u.dst.dev;
	skb_dst_set(skb, (struct dst_entry *)rt);
		
	if (skb->len > ip_skb_dst_mtu(skb) && !skb_is_gso(skb)) {
		return ip_fragment(skb, nativi_output_ipv4_2);
	} else {
		if(ip_local_out(skb)) {
		       printk("nf_nativi: ip_local_out failed\n");
		       return -1;
		}
	}
		
	return 0;
}

/* static void */
/* nativi_output_ipv6(struct sk_buff *skb) */
/* { */
	/* skb->protocol = htons(ETH_P_IPV6); */
	/* skb->dev = nativi_dev; */
	/* nativi_dev->stats.tx_packets++; */
	/* nativi_dev->stats.tx_bytes += skb->len; */
	/* netif_rx(skb); */
/* } */

static int
nativi_output_ipv6_3(struct sk_buff *skb)
{
	struct flowi fl;
	struct ipv6hdr *iph = ipv6_hdr(skb);
	struct dst_entry *dst;

	skb->protocol = htons(ETH_P_IPV6);

	memset(&fl, 0, sizeof(fl));
	fl.fl6_dst = iph->daddr;
	fl.fl6_flowlabel = 0;
	fl.proto = skb->protocol;

	if (!(dst = ip6_route_output(&init_net, NULL, &fl)))
	{
		printk("nf_nativi: ip_route_output_key failed\n");
		return -1;
	}


	skb->dev = dst->dev;
	skb_dst_set(skb, dst);

	return ip6_local_out(skb);
}

static int
nativi_output_ipv6_2(struct sk_buff *skb)
{
	struct flowi fl;
	struct ipv6hdr *iph = ipv6_hdr(skb);
	struct dst_entry *dst;

	skb->protocol = htons(ETH_P_IPV6);

	memset(&fl, 0, sizeof(fl));
	fl.fl6_dst = iph->daddr;
	fl.fl6_flowlabel = 0;
	fl.proto = skb->protocol;

	if (!(dst = ip6_route_output(&init_net, NULL, &fl)))
	{
		printk("nf_nativi: ip_route_output_key failed\n");
		return -1;
	}


	skb->dev = dst->dev;
	skb_dst_set(skb, dst);

    /* if ((skb->len > ip6_skb_dst_mtu(skb) && !skb_is_gso(skb)) || */
        /* dst_allfrag(skb_dst(skb))) */
	if ((skb->len > ip6_skb_dst_mtu(skb))) {
		return ip6_fragment(skb, nativi_output_ipv6_3);
	} else
		return ip6_local_out(skb);
}

static int
is_frag6(struct ipv6hdr *ip6, int len)
{
	struct ipv6_opt_hdr     *ip6e;
	struct frag_hdr		*frag_header;
	uint16_t		 hlen;
	uint8_t			 proto;

	hlen  = sizeof(*ip6);
	proto = ip6->nexthdr;

next_header:
	switch (proto) {

	case IPPROTO_HOPOPTS:
	case IPPROTO_ROUTING:
	case IPPROTO_DSTOPTS:
		ip6e = (struct ipv6_opt_hdr*)((char *)ip6 + hlen);

		if ((len -= sizeof(*ip6e)) < 0)
			return 0;

		proto = ip6e->nexthdr;
		hlen += ip6e->hdrlen * 8;

		goto next_header;

	case IPPROTO_FRAGMENT:
		/* The kernel should be handling fragments for us. DROP. */
//		WARN_ON_ONCE(1);
//		return 0;
		
		frag_header = (struct frag_hdr *)((char *)ip6 + hlen);

		proto = frag_header->nexthdr;
		hlen += 8;

		if ((len -= sizeof(struct frag_hdr)) < 0)
			return 0;

		// if is not first fragment return len
		if (frag_header->frag_off & htons(0xFFF9)) {
			return 1;
		} else {
			goto next_header;
		}

	case IPPROTO_UDP:
	case IPPROTO_TCP:
	case IPPROTO_ICMPV6:
		return 0;
	default:
		/* Ignore other protocols. */
		return 0;
	}

	/* Should never get here. */
	WARN_ON_ONCE(1);
	return 0;
}

static unsigned int
nativi_input_ipv6(struct sk_buff *skb, struct net_device *dev)
{
	struct iphdr *ip4;
	struct ipv6hdr *ip6;
	struct in6_addr *daddr;
//	struct nativi_session *s;
	struct sk_buff *nskb;
    struct sk_buff *reasm = NULL;

	int len = skb->len;
	int plen;

	if ((len -= sizeof(*ip6)) < 0)
		return -1;
	struct in6_addr taddr;
	memset(&taddr, 0, 16);
	taddr.s6_addr16[0]=0xffff;

	ip6 = ipv6_hdr(skb);
	daddr = &ip6->daddr;

	/* Match only the nativi_prefix */
	//set original config to 1
	/* isolate code
	if (memcmp(daddr, nativi_config_prefix(1, 2), nativi_config_prefix_len(1, 2)/8) == 0) {
		nativi_set_config(2);
	} else if (memcmp(daddr, nativi_config_prefix(1, 1), nativi_config_prefix_len(1, 1)/8) == 0) {
		nativi_set_config(1);
	} else {
		return -1;
	}
*/
	nativi_set_config(1);

	/* Check for expired sessions */
//	nativi_expire();

	/* do defrag here */
	if (is_frag6(ip6, len)) {
		reasm = nf_ct_frag6_gather(skb, IP_DEFRAG_NAT64);
		if (reasm == NULL) {
			return 0;
		}
		if (reasm != skb) {
			struct sk_buff *s,*s2;
			for (s = NFCT_FRAG6_CB(reasm)->orig; s;) {
				kfree_skb(s);
				s2 = s->next;
				s->next = NULL;
				s = s2;
			}
			skb = reasm;
			if (skb_linearize(skb)) {
				printk("nf_nativi: Can't linearize skb!\n");
				return -1;
			}
			ip6 = ipv6_hdr(skb);
			len = skb->len;
			if ((len -= sizeof(*ip6)) < 0)
				return -1;
			daddr = &ip6->daddr;
		}
	}
			

	/* Find the corresponding session. Create one if none exist. */
	if (!(plen = nativi_input_ipv6_recur(0, ip6, len)))
		return -1;

	/* Allocate a new sk_buff */
	nskb = nativi_alloc_skb(sizeof(struct iphdr), plen);
	ip4 = ip_hdr(nskb);

	if(!nskb) {
		if(printk_ratelimit())
			printk(KERN_DEBUG "nat_nativi: can't alloc a new skb\n");
		return -1;
	}

	/* Translate the packet. */
	if (!nativi_xlate_ipv6_to_ipv4(ip6, ip4, plen, 0)) {
		kfree_skb(nskb);
		return -1;
	}


	nativi_output_ipv4(nskb);

	/* Free the incoming packet */
	kfree_skb(skb);
	return 0;
}

static int
nativi_input_ipv4_recur(int recur, struct iphdr *ip4, int len)
{
	struct udphdr           *uh;
	struct tcphdr           *th;
	struct icmphdr *icmp;
	uint16_t hlen;

	hlen = ip4->ihl * 4;

	switch (ip4->protocol) {

	case IPPROTO_UDP:
		uh = (struct udphdr *)((char *)ip4 + hlen);

		if ((len -= sizeof(*uh)) < 0)
			return 0;

		return len + sizeof(*uh);

	case IPPROTO_TCP:
		th = (struct tcphdr *)((char *)ip4 + hlen);

		if((len -= sizeof(*th)) < 0)
			return 0;

		return len + sizeof(*th);

	case IPPROTO_ICMP:
		icmp = (struct icmphdr *)((char *)ip4 + hlen);

		if ((len -= ICMP_MINLEN) < 0)
			return 0;

		if (ICMP_INFOTYPE(icmp->type)) {
			return len + sizeof(struct icmp6hdr);
		} else {
			len = nativi_input_ipv4_recur(recur + 1,
					(struct iphdr*)(icmp + 1),
					len - sizeof(*ip4));
			return len ? len + sizeof(struct ipv6hdr) +
				sizeof(struct icmp6hdr) : 0;
		}

	default:
		/* Ignore other protocols. */
		return 0;
	}

	/* Should never get here. */
	WARN_ON_ONCE(1);
	return 0;
}

static struct ipv6hdr *
nativi_xlate_ipv4_to_ipv6(struct iphdr *ip4, struct ipv6hdr *ip6, int plen,
		int recur)
{
	struct udphdr		*uh;
	struct tcphdr		*th;
	struct icmp6hdr *icmp6;

	ip6->version = 6;
	ip6->priority = 0;
	ip6->flow_lbl[0] = 0;
	ip6->flow_lbl[1] = 0;
	ip6->flow_lbl[2] = 0;

	ip6->payload_len = htons(plen);
	ip6->nexthdr  = ip4->protocol;
	ip6->hop_limit = ip4->ttl - 1;

		nativi_embed(*(struct in_addr *)&ip4->saddr, &ip6->saddr, 1);
		nativi_embed(*(struct in_addr *)&ip4->daddr, &ip6->daddr, 0);

	switch(ip6->nexthdr) {
	case IPPROTO_UDP:
		uh = (struct udphdr *)(ip6 + 1);
		memcpy(uh, ip_data(ip4), plen);
//		checksum_change(&uh->check, recur % 2 ? &uh->source :
//				&uh->dest, s->s_binding->b_sport6, 1);
		if(uh->check) {
			adjust_checksum_ipv4_to_ipv6(&uh->check, ip4, ip6, 1);
		} else {
			uh->check = csum_ipv6_magic(
					&ip6->saddr, &ip6->daddr,
					plen, IPPROTO_UDP,
					csum_partial(uh, plen, 0));
		}
		break;
	case IPPROTO_TCP:
		th = (struct tcphdr *)(ip6 + 1);
		memcpy(th, ip_data(ip4), plen);
//		checksum_change(&th->check, recur % 2 ? &th->source :
//				&th->dest, s->s_binding->b_sport6, 0);
		adjust_checksum_ipv4_to_ipv6(&th->check, ip4, ip6, 0);
		break;
	case IPPROTO_ICMP:
		icmp6 = (struct icmp6hdr *)(ip6 + 1);
		memcpy(icmp6, ip_data(ip4), plen);
		if (ICMP_INFOTYPE(icmp6->icmp6_type)) {
			switch (icmp6->icmp6_type) {
			case ICMP_ECHO:
				icmp6->icmp6_type = ICMPV6_ECHO_REQUEST;
				break;
			case ICMP_ECHOREPLY:
				icmp6->icmp6_type = ICMPV6_ECHO_REPLY;
				break;
			default:
				return NULL;
			}
		} else {
			switch (icmp6->icmp6_type) {
			case ICMP_DEST_UNREACH:
				icmp6->icmp6_type = ICMPV6_DEST_UNREACH;
				switch (icmp6->icmp6_code) {
				case ICMP_NET_UNREACH:
				case ICMP_HOST_UNREACH:
					icmp6->icmp6_code = ICMPV6_NOROUTE;
					break;
				case ICMP_PORT_UNREACH:
					icmp6->icmp6_code = ICMPV6_PORT_UNREACH;
					break;
				case ICMP_SR_FAILED:
				case ICMP_NET_UNKNOWN:
				case ICMP_HOST_UNKNOWN:
				case ICMP_HOST_ISOLATED:
				case ICMP_NET_UNR_TOS:
				case ICMP_HOST_UNR_TOS:
					icmp6->icmp6_code = ICMPV6_NOROUTE;
					break;
				case ICMP_NET_ANO:
				case ICMP_HOST_ANO:
					icmp6->icmp6_code =
						ICMPV6_ADM_PROHIBITED;
					break;
				case ICMP_PROT_UNREACH:
					icmp6->icmp6_type = ICMPV6_PARAMPROB;
					icmp6->icmp6_code =
						ICMPV6_UNK_NEXTHDR;
					icmp6->icmp6_pointer =
						(char *)&ip6->nexthdr -
						(char *)ip6;
					break;
				case ICMP_FRAG_NEEDED:
					icmp6->icmp6_type = ICMPV6_PKT_TOOBIG;
					icmp6->icmp6_code = 0;
					icmp6->icmp6_mtu = htonl(ntohl(icmp6->icmp6_mtu)+20);
					/* TODO handle icmp_nextmtu == 0 */
					break;
				default:
					return NULL;
				}
				break;
			case ICMP_TIME_EXCEEDED:
				icmp6->icmp6_type = ICMPV6_TIME_EXCEED;
				break;
			case ICMP_PARAMETERPROB:
				icmp6->icmp6_type = ICMPV6_PARAMPROB;
				/* TODO update pointer */
				break;
			default:
				return NULL;
			}
			nativi_xlate_ipv4_to_ipv6(ip_data(ip4) + 8,
					(struct ipv6hdr *)(icmp6 + 1),
					plen - sizeof(*icmp6) - sizeof(*ip6),
					recur + 1);
		}
		icmp6->icmp6_cksum = 0;
		ip6->nexthdr = IPPROTO_ICMPV6;
		icmp6->icmp6_cksum = csum_ipv6_magic(&ip6->saddr, &ip6->daddr,
				plen, IPPROTO_ICMPV6,
				csum_partial(icmp6, plen, 0));
		break;
	default:
		WARN_ON_ONCE(1);
	}

	return ip6;
}

static int nativi_match_addr(__be32 ipaddr, struct in_addr *nat_addr, int mask)
{
	__be32 m;
	if (mask == 0) {
		m = 0;
	} else {
		m = 0x80000000;
		while(--mask) {
			m |= m >> 1;
		}
	}
	ipaddr = ipaddr & htonl(m);		//notice that daddr is in network edian
	return memcmp(&ipaddr, nat_addr, 4);
}

static int nativi_to_local(__be32 ipaddr)
{
	struct in_ifaddr *ifap;
	struct net_device *dev;
	struct in_device *in_dev;

	dev = first_net_device(&init_net);
	while (dev) {
		//printk(KERN_ALERT "dev->name:%s\n",dev->name);
		in_dev = rcu_dereference(dev->ip_ptr);
		for (ifap = in_dev->ifa_list; ifap != NULL; 
		ifap = ifap->ifa_next) {
			//printk(KERN_ALERT "cmp %x to %x\n",ipaddr,ifap->ifa_address);
			if (ifap->ifa_address == ipaddr)
				return -1;
		}
		dev = next_net_device(dev);
	}
	return 0;
}

static unsigned int
/*
nativi_input_ipv4(unsigned int hooknum,
		struct sk_buff *skb,
		const struct net_device *in,
		const struct net_device *out,
		int (*okfn)(struct sk_buff *))
*/
nativi_input_ipv4(struct sk_buff *skb, struct net_device *dev)
{
//	struct nativi_session *s;
	struct iphdr *ip4 = ip_hdr(skb);
	struct ipv6hdr *ip6;
	struct sk_buff *nskb;
	int len = skb->len;
	int plen;

/*	if (nativi_to_local(ip4->daddr))
		return NF_ACCEPT; */
	if ((len -= sizeof(*ip4)) < 0)
		return -1;

	/* defrag fragment packet */
	if (ip4->frag_off & htons(IP_MF | IP_OFFSET)) {
		if (ip_defrag(skb, IP_DEFRAG_NAT64)) {
			return 0;
		}
		if(skb_linearize(skb)) {
            printk("nf_nativi:Can't linearize skb!\n");
            return -1;
        }
		ip4 = ip_hdr(skb);
		len = skb->len;
		if ((len -= sizeof(*ip4)) < 0)
			return -1;
	}

	/* Match only the nativi_addr */
	/*
	if (((nativi_config_nat_addr(2))->s_addr != 0x0100007f) &&
	(nativi_match_addr(ip4->daddr, nativi_config_nat_addr(2), 
	nativi_config_nat_mask(2)) == 0)) {
			nativi_set_config(2);
	} else if (nativi_match_addr(ip4->daddr, nativi_config_nat_addr(1), nativi_config_nat_mask(1)) == 0) {
			nativi_set_config(1);
	} else {
			return -1;
	}
	*/

	nativi_set_config(1);
	/* Check for expired sessions */
//	nativi_expire();

	/* Find the corresponding session. Create one if none exist. */
	if(!(plen = nativi_input_ipv4_recur(0, ip_hdr(skb), len)))
		return -1;

	/* Allocate a new sk_buff */
	nskb = nativi_alloc_skb(sizeof(struct ipv6hdr), plen);

	if(!nskb) {
		if(printk_ratelimit())
			printk(KERN_DEBUG "nat_nativi: can't alloc a new skb\n");
		return -1;
	}

	ip6 = ipv6_hdr(nskb);

	/* Translate the packet. */
	if (!nativi_xlate_ipv4_to_ipv6(ip4, ip6, plen, 0)) {
		kfree_skb(nskb);
		return -1;
	}

	nativi_output_ipv6_2(nskb);

	kfree_skb(skb);
	return 0;
}
/*
static struct nf_hook_ops nf_nativi_ops[] __read_mostly = {
	{
		.hook           = nativi_input_ipv4,
		.owner          = THIS_MODULE,
		.pf             = NFPROTO_IPV4,
		//.hooknum        = NF_INET_LOCAL_IN,
		.hooknum        = NF_INET_PRE_ROUTING,
		// .priority       = NF_IP_PRI_NAT_SRC,
		.priority       = NF_IP_PRI_FIRST,
	}
};
*/

static int __init nativi_init_config(void)
{
	struct in_addr ipv4_addr;
	struct in6_addr prefix;

	int ret = 0;
	ret = in6_pton(nativi_src_prefix_addr, -1,
			(u8*) &(prefix.in6_u.u6_addr8),
			'\x0', NULL);
	if (!ret) {
		printk(KERN_INFO "nf_nativi: can't init src prefix\n");
		return -EINVAL;
	}

	nativi_config_set_prefix(prefix, nativi_src_prefix_len, 1, 1);

	if(nativi_src_prefix_len % 8) {
		printk(KERN_INFO "nf_nativi: nativi_src_prefix_len must be a multiple of 8\n");
		return -EINVAL;
	}

	ret = 0;
	ret = in6_pton(nativi_dst_prefix_addr, -1,
			(u8*) &(prefix.in6_u.u6_addr8),
			'\x0', NULL);
	if (!ret) {
		printk(KERN_INFO "nf_nativi: can't init dst prefix\n");
		return -EINVAL;
	}

	nativi_config_set_prefix(prefix, nativi_dst_prefix_len, 0, 1);

	if(nativi_dst_prefix_len % 8) {
		printk(KERN_INFO "nf_nativi: nativi_dst_prefix_len must be a multiple of 8\n");
		return -EINVAL;
	}

//	if(nativi_ipv4_addr == NULL) {
//		printk(KERN_INFO "nf_nativi: module parameter \'nativi_ipv4_addr\' is undefined\n");
//		return -EINVAL;
//	}

	/* lynx */
//	if (nativi_ipv4_mask > 32 || nativi_ipv4_mask < 0) {
//		printk(KERN_INFO "nf_nativi: IPV4_MASK mast less than 32 and bigger than 0\n");
//		return -EINVAL;
//	}

//	ret = in4_pton(nativi_ipv4_addr, -1, (u8*)&(ipv4_addr.s_addr),
//			'\x0', NULL);

	/* XXX with ip_route_output_key we can determine the right address */
	/* lynx*/
//	nativi_config_set_nat_addr(ipv4_addr, nativi_ipv4_mask, 1);

	return 0;
}



static int
nativi_get_settings(struct net_device *dev, struct ethtool_cmd *cmd)
{
        cmd->supported          = 0;
        cmd->advertising        = 0;
        cmd->speed              = SPEED_10;
        cmd->duplex             = DUPLEX_FULL;
        cmd->port               = PORT_TP;
        cmd->phy_address        = 0;
        cmd->transceiver        = XCVR_INTERNAL;
        cmd->autoneg            = AUTONEG_DISABLE;
        cmd->maxtxpkt           = 0;
        cmd->maxrxpkt           = 0;
        return 0;
}

static void
nativi_get_drvinfo(struct net_device *dev, struct ethtool_drvinfo *info)
{
        strcpy(info->driver, NAT64_NETDEV_NAME);
        strcpy(info->version, NAT64_VERSION);
        strcpy(info->fw_version, "N/A");

        strcpy(info->bus_info, "nativi");
}

static const struct ethtool_ops nativi_ethtool_ops = {
        .get_settings   = nativi_get_settings,
        .get_drvinfo    = nativi_get_drvinfo,
};

static int nativi_netdev_open(struct net_device *dev)
{
        netif_start_queue(dev);
        return 0;
}

static int nativi_netdev_close(struct net_device *dev)
{
        netif_stop_queue(dev);
        return 0;
}

static int
nativi_netdev_xmit(struct sk_buff *skb, struct net_device *dev)
{
        struct ipv6hdr *ip6 = ipv6_hdr(skb);
	struct iphdr *iph = ip_hdr(skb);
        uint16_t len = skb->len;

	/*
        if(ip6->version != 6) {
            goto drop;
        }

        if(nativi_input_ipv6(skb, dev)) {
            goto drop;
        }
	*/
	if(ip6->version == 6) {
		if(nativi_input_ipv6(skb, dev)) {
			goto drop;
		}
	} else if (iph->version == 4) {
		if (nativi_input_ipv4(skb, dev)) {
			goto drop;
		}
	}

        dev->stats.rx_packets++;
        dev->stats.rx_bytes += len;
        return NETDEV_TX_OK;

drop:
        dev->stats.rx_dropped++;
        kfree_skb(skb);
        return NETDEV_TX_OK;

}

static const struct net_device_ops nativi_netdev_ops = {
        .ndo_open               = nativi_netdev_open,
        .ndo_stop               = nativi_netdev_close,
        .ndo_start_xmit         = nativi_netdev_xmit,
};

static void nativi_free_netdev(struct net_device *dev)
{
}

static void nativi_netdev_setup(struct net_device *dev)
{
        dev->ethtool_ops = &nativi_ethtool_ops;
        dev->netdev_ops = &nativi_netdev_ops;

        dev->hard_header_len = 0;
        dev->addr_len = 0;
        dev->mtu = 1500;

        dev->type = ARPHRD_NONE;
        dev->flags = IFF_POINTOPOINT | IFF_NOARP | IFF_MULTICAST;

        dev->destructor = nativi_free_netdev;
}

/* XXX ??? */
static int nativi_netdev_validate(struct nlattr *tb[], struct nlattr *data[])
{
	return -EINVAL;
}

static struct rtnl_link_ops nativi_link_ops __read_mostly = {
	.kind           = NAT64_NETDEV_NAME,
	.priv_size      = sizeof(struct nativi_struct),
	.setup          = nativi_netdev_setup,
	.validate       = nativi_netdev_validate,
};

int nativi_netdev_init(void)
{
        struct nativi_struct *nativi;
	struct net_device *dev;
	int err = 0;
	err = rtnl_link_register(&nativi_link_ops);
	if (err) {
		printk(KERN_ERR "nf_nativi: Can't register link_ops\n");
	}

	dev = alloc_netdev(sizeof(struct nativi_struct), NAT64_NETDEV_NAME,
			nativi_netdev_setup);
	if (!dev)
		return -ENOMEM;

        nativi = netdev_priv(dev);
	nativi->dev = dev;
	nativi_dev = dev;

	dev_net_set(dev, &init_net);
	dev->rtnl_link_ops = &nativi_link_ops;

	return register_netdev(dev);
}

void nativi_netdev_uninit(void)
{
	rtnl_link_unregister(&nativi_link_ops);
}

static int __init nativi_init(void)
{
	int err = 0;

	err = nativi_init_config();
	nativi_set_config(1);
	if(err) {
		return err;
	} else {
		printk(KERN_INFO "nf_nativi: nativi_src_prefix=%pI6c/%d\n",
			nativi_config_prefix(1, 1), nativi_config_prefix_len(1, 1));
		printk(KERN_INFO "nf_nativi: nativi_dst_prefix=%pI6c/%d\n",
			nativi_config_prefix(0, 1), nativi_config_prefix_len(0, 1));
	}

	err = nativi_netdev_init();
	if(err) {
		return err;
	}

	err = nf_ct_frag6_init();
	if (err < 0) {
		printk("nf_nativi: can't initialize frag6.\n");
		return err;
	}

	//ret = nf_register_hooks(nf_nativi_ops, ARRAY_SIZE(nf_nativi_ops));
	/*
	err = nf_register_hook(nf_nativi_ops);
	if(err) {
        nf_ct_frag6_cleanup();
		printk("nf_nativi: can't register hooks.\n");
	}
	*/
	return 0;
}

static void __exit nativi_fini(void)
{
	//nf_unregister_hooks(nf_nativi_ops, ARRAY_SIZE(nf_nativi_ops));
	//nf_unregister_hook(nf_nativi_ops);
	nf_ct_frag6_cleanup();
	nativi_netdev_uninit();
	printk(KERN_INFO "nf_nativi: module removed\n");
}

module_init(nativi_init);
module_exit(nativi_fini);


