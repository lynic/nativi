#!/bin/bash

nativi-start
ip route add default dev nativi table 177
ip rule del fwmark 177 table 177
ip rule add fwmark 177 table 177
iptables -t mangle -F
iptables -t mangle -A PREROUTING -j CONNMARK --restore-mark
iptables -t mangle -A PREROUTING -m mark ! --mark 0 -j ACCEPT
iptables -t mangle -A PREROUTING -s 10.0.0.0/24 -j MARK --set-mark 177
iptables -t mangle -A PREROUTING -j CONNMARK --save-mark
ip route flush cache
iptables -t mangle -I POSTROUTING -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1440
