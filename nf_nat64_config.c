/*
 * NAT64 - Network Address Translator IPv6 to IPv4
 *
 * Copyright (C) 2010 Viagenie Inc. http://www.viagenie.ca
 *
 * Authors:
 *      Jean-Philippe Dionne <jean-philippe.dionne@viagenie.ca>
 *      Simon Perreault <simon.perreault@viagenie.ca>
 *      Marc Blanchet <marc.blanchet@viagenie.ca>
 *
 * NAT64 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NAT64 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with NAT64.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "linux/bug.h"
#include "nf_nat64_config.h"

#include <linux/kernel.h>	/* Needed for KERN_INFO */
/* lynx */
#include <linux/random.h>


struct nativi_config_t {
	int src_prefix_len;
	struct in6_addr src_prefix;
	int dst_prefix_len;
	struct in6_addr dst_prefix;
	/* XXX Determine this address dynamicaly */
//	struct in_addr nat_addr;
//	int ipv4_mask;
};

static struct nativi_config_t nativi_config[3];

/*
the value of int num must be 1 or 2
*/
void nativi_set_config(int num)
{
	nativi_config[0].src_prefix_len=nativi_config[num].src_prefix_len;
	nativi_config[0].src_prefix=nativi_config[num].src_prefix;
	nativi_config[0].dst_prefix_len=nativi_config[num].dst_prefix_len;
	nativi_config[0].dst_prefix=nativi_config[num].dst_prefix;
//	nativi_config[0].nat_addr=nativi_config[num].nat_addr;
//	nativi_config[0].ipv4_mask=nativi_config[num].ipv4_mask;
}

void nativi_config_set_prefix(struct in6_addr prefix, int prefix_len, int isSrc, int num)
{
	if (isSrc) {
		if (prefix_len == 96 && nativi_config[num].src_prefix.s6_addr[8] == 0)
			printk(KERN_ERR "nf_nativi: Bits 64 to 71 must be set to 0. See draft-ietf-behave-address-format.\n");
		nativi_config[num].src_prefix = prefix;
		nativi_config[num].src_prefix_len = prefix_len;
	} else {
		if (prefix_len == 96 && nativi_config[num].dst_prefix.s6_addr[8] == 0)
			printk(KERN_ERR "nf_nativi: Bits 64 to 71 must be set to 0. See draft-ietf-behave-address-format.\n");
		nativi_config[num].dst_prefix = prefix;
		nativi_config[num].dst_prefix_len = prefix_len;
	}
}

/* lynx */
/* isolated function
void nativi_config_set_nat_addr(struct in_addr addr, int mask, int num)
{
	//ignore bits out of mask
	nativi_config[num].ipv4_mask = mask;

	__be32 m;
	if (mask == 0) {
		m = 0;
	} else {
		m = 0x80000000;
		while(--mask) {
			m |= m >> 1;
		}
	}
	addr.s_addr = addr.s_addr & htonl(m);
	nativi_config[num].nat_addr = addr;
}
*/

int nativi_config_prefix_len(int isSrc, int num)
{
	return isSrc?nativi_config[num].src_prefix_len:nativi_config[num].dst_prefix_len;
}

struct in6_addr * nativi_config_prefix(int isSrc, int num)
{
	if (isSrc)
		return &(nativi_config[num].src_prefix);
	else
		return &(nativi_config[num].dst_prefix);
}

/* isolated function
struct in_addr * nativi_config_nat_addr(int num)
{
	return &(nativi_config[num].nat_addr);
	//return pick_addr_from_pool(nativi_config.nat_addr, nativi_config.ipv4_mask);
}
*/

/* isolated function
int nativi_config_nat_mask(int num)
{
	return nativi_config[num].ipv4_mask;
}
*/


struct in_addr
nativi_extract(const struct in6_addr *a6, int isSrc)
{
	struct in_addr           a4;

	switch (isSrc?nativi_config[0].src_prefix_len:nativi_config[0].dst_prefix_len) {
	case 32:
		((uint8_t *)&a4)[0] = a6->s6_addr[4];
		((uint8_t *)&a4)[1] = a6->s6_addr[5];
		((uint8_t *)&a4)[2] = a6->s6_addr[6];
		((uint8_t *)&a4)[3] = a6->s6_addr[7];
		break;
	case 40:
		((uint8_t *)&a4)[0] = a6->s6_addr[5];
		((uint8_t *)&a4)[1] = a6->s6_addr[6];
		((uint8_t *)&a4)[2] = a6->s6_addr[7];
		((uint8_t *)&a4)[3] = a6->s6_addr[9];
		break;
	case 48:
		((uint8_t *)&a4)[0] = a6->s6_addr[6];
		((uint8_t *)&a4)[1] = a6->s6_addr[7];
		((uint8_t *)&a4)[2] = a6->s6_addr[9];
		((uint8_t *)&a4)[3] = a6->s6_addr[10];
		break;
	case 56:
		((uint8_t *)&a4)[0] = a6->s6_addr[7];
		((uint8_t *)&a4)[1] = a6->s6_addr[9];
		((uint8_t *)&a4)[2] = a6->s6_addr[10];
		((uint8_t *)&a4)[3] = a6->s6_addr[11];
		break;
	case 64:
		((uint8_t *)&a4)[0] = a6->s6_addr[9];
		((uint8_t *)&a4)[1] = a6->s6_addr[10];
		((uint8_t *)&a4)[2] = a6->s6_addr[11];
		((uint8_t *)&a4)[3] = a6->s6_addr[12];
		break;
	case 96:
		((uint8_t *)&a4)[0] = a6->s6_addr[12];
		((uint8_t *)&a4)[1] = a6->s6_addr[13];
		((uint8_t *)&a4)[2] = a6->s6_addr[14];
		((uint8_t *)&a4)[3] = a6->s6_addr[15];
		break;
		// XXX kernel equivalent? */
	default:
		WARN_ON_ONCE(1);
	}

	return a4;
}

void
nativi_embed(struct in_addr a4, struct in6_addr *a6, int isSrc)
{
	if (isSrc) {
		switch (nativi_config[0].src_prefix_len) {
		case 32:
			a6->s6_addr[ 0] = nativi_config[0].src_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].src_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].src_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].src_prefix.s6_addr[3];
			a6->s6_addr[ 4] = ((uint8_t *)&a4)[0];
			a6->s6_addr[ 5] = ((uint8_t *)&a4)[1];
			a6->s6_addr[ 6] = ((uint8_t *)&a4)[2];
			a6->s6_addr[ 7] = ((uint8_t *)&a4)[3];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = 0;
			a6->s6_addr[10] = 0;
			a6->s6_addr[11] = 0;
			a6->s6_addr[12] = 0;
			a6->s6_addr[13] = 0;
			a6->s6_addr[14] = 0;
			a6->s6_addr[15] = 0;
			break;
		case 40:
			a6->s6_addr[ 0] = nativi_config[0].src_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].src_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].src_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].src_prefix.s6_addr[3];
			a6->s6_addr[ 4] = nativi_config[0].src_prefix.s6_addr[4];
			a6->s6_addr[ 5] = ((uint8_t *)&a4)[0];
			a6->s6_addr[ 6] = ((uint8_t *)&a4)[1];
			a6->s6_addr[ 7] = ((uint8_t *)&a4)[2];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = ((uint8_t *)&a4)[3];
			a6->s6_addr[10] = 0;
			a6->s6_addr[11] = 0;
			a6->s6_addr[12] = 0;
			a6->s6_addr[13] = 0;
			a6->s6_addr[14] = 0;
			a6->s6_addr[15] = 0;
			break;
		case 48:
			a6->s6_addr[ 0] = nativi_config[0].src_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].src_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].src_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].src_prefix.s6_addr[3];
			a6->s6_addr[ 4] = nativi_config[0].src_prefix.s6_addr[4];
			a6->s6_addr[ 5] = nativi_config[0].src_prefix.s6_addr[5];
			a6->s6_addr[ 6] = ((uint8_t *)&a4)[0];
			a6->s6_addr[ 7] = ((uint8_t *)&a4)[1];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = ((uint8_t *)&a4)[2];
			a6->s6_addr[10] = ((uint8_t *)&a4)[3];
			a6->s6_addr[11] = 0;
			a6->s6_addr[12] = 0;
			a6->s6_addr[13] = 0;
			a6->s6_addr[14] = 0;
			a6->s6_addr[15] = 0;
			break;
		case 56:
			a6->s6_addr[ 0] = nativi_config[0].src_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].src_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].src_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].src_prefix.s6_addr[3];
			a6->s6_addr[ 4] = nativi_config[0].src_prefix.s6_addr[4];
			a6->s6_addr[ 5] = nativi_config[0].src_prefix.s6_addr[5];
			a6->s6_addr[ 6] = nativi_config[0].src_prefix.s6_addr[6];
			a6->s6_addr[ 7] = ((uint8_t *)&a4)[0];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = ((uint8_t *)&a4)[1];
			a6->s6_addr[10] = ((uint8_t *)&a4)[2];
			a6->s6_addr[11] = ((uint8_t *)&a4)[3];
			a6->s6_addr[12] = 0;
			a6->s6_addr[13] = 0;
			a6->s6_addr[14] = 0;
			a6->s6_addr[15] = 0;
			break;
		case 64:
			a6->s6_addr[ 0] = nativi_config[0].src_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].src_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].src_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].src_prefix.s6_addr[3];
			a6->s6_addr[ 4] = nativi_config[0].src_prefix.s6_addr[4];
			a6->s6_addr[ 5] = nativi_config[0].src_prefix.s6_addr[5];
			a6->s6_addr[ 6] = nativi_config[0].src_prefix.s6_addr[6];
			a6->s6_addr[ 7] = nativi_config[0].src_prefix.s6_addr[7];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = ((uint8_t *)&a4)[0];
			a6->s6_addr[10] = ((uint8_t *)&a4)[1];
			a6->s6_addr[11] = ((uint8_t *)&a4)[2];
			a6->s6_addr[12] = ((uint8_t *)&a4)[3];
			a6->s6_addr[13] = 0;
			a6->s6_addr[14] = 0;
			a6->s6_addr[15] = 0;
			break;
		case 96:
			a6->s6_addr[ 0] = nativi_config[0].src_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].src_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].src_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].src_prefix.s6_addr[3];
			a6->s6_addr[ 4] = nativi_config[0].src_prefix.s6_addr[4];
			a6->s6_addr[ 5] = nativi_config[0].src_prefix.s6_addr[5];
			a6->s6_addr[ 6] = nativi_config[0].src_prefix.s6_addr[6];
			a6->s6_addr[ 7] = nativi_config[0].src_prefix.s6_addr[7];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = nativi_config[0].src_prefix.s6_addr[9];
			a6->s6_addr[10] = nativi_config[0].src_prefix.s6_addr[10];
			a6->s6_addr[11] = nativi_config[0].src_prefix.s6_addr[11];
			a6->s6_addr[12] = ((uint8_t *)&a4)[0];
			a6->s6_addr[13] = ((uint8_t *)&a4)[1];
			a6->s6_addr[14] = ((uint8_t *)&a4)[2];
			a6->s6_addr[15] = ((uint8_t *)&a4)[3];
			break;
		default:
			WARN_ON_ONCE(1);
		}
	}else {
		switch (nativi_config[0].dst_prefix_len) {
		case 32:
			a6->s6_addr[ 0] = nativi_config[0].dst_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].dst_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].dst_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].dst_prefix.s6_addr[3];
			a6->s6_addr[ 4] = ((uint8_t *)&a4)[0];
			a6->s6_addr[ 5] = ((uint8_t *)&a4)[1];
			a6->s6_addr[ 6] = ((uint8_t *)&a4)[2];
			a6->s6_addr[ 7] = ((uint8_t *)&a4)[3];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = 0;
			a6->s6_addr[10] = 0;
			a6->s6_addr[11] = 0;
			a6->s6_addr[12] = 0;
			a6->s6_addr[13] = 0;
			a6->s6_addr[14] = 0;
			a6->s6_addr[15] = 0;
			break;
		case 40:
			a6->s6_addr[ 0] = nativi_config[0].dst_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].dst_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].dst_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].dst_prefix.s6_addr[3];
			a6->s6_addr[ 4] = nativi_config[0].dst_prefix.s6_addr[4];
			a6->s6_addr[ 5] = ((uint8_t *)&a4)[0];
			a6->s6_addr[ 6] = ((uint8_t *)&a4)[1];
			a6->s6_addr[ 7] = ((uint8_t *)&a4)[2];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = ((uint8_t *)&a4)[3];
			a6->s6_addr[10] = 0;
			a6->s6_addr[11] = 0;
			a6->s6_addr[12] = 0;
			a6->s6_addr[13] = 0;
			a6->s6_addr[14] = 0;
			a6->s6_addr[15] = 0;
			break;
		case 48:
			a6->s6_addr[ 0] = nativi_config[0].dst_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].dst_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].dst_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].dst_prefix.s6_addr[3];
			a6->s6_addr[ 4] = nativi_config[0].dst_prefix.s6_addr[4];
			a6->s6_addr[ 5] = nativi_config[0].dst_prefix.s6_addr[5];
			a6->s6_addr[ 6] = ((uint8_t *)&a4)[0];
			a6->s6_addr[ 7] = ((uint8_t *)&a4)[1];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = ((uint8_t *)&a4)[2];
			a6->s6_addr[10] = ((uint8_t *)&a4)[3];
			a6->s6_addr[11] = 0;
			a6->s6_addr[12] = 0;
			a6->s6_addr[13] = 0;
			a6->s6_addr[14] = 0;
			a6->s6_addr[15] = 0;
			break;
		case 56:
			a6->s6_addr[ 0] = nativi_config[0].dst_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].dst_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].dst_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].dst_prefix.s6_addr[3];
			a6->s6_addr[ 4] = nativi_config[0].dst_prefix.s6_addr[4];
			a6->s6_addr[ 5] = nativi_config[0].dst_prefix.s6_addr[5];
			a6->s6_addr[ 6] = nativi_config[0].dst_prefix.s6_addr[6];
			a6->s6_addr[ 7] = ((uint8_t *)&a4)[0];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = ((uint8_t *)&a4)[1];
			a6->s6_addr[10] = ((uint8_t *)&a4)[2];
			a6->s6_addr[11] = ((uint8_t *)&a4)[3];
			a6->s6_addr[12] = 0;
			a6->s6_addr[13] = 0;
			a6->s6_addr[14] = 0;
			a6->s6_addr[15] = 0;
			break;
		case 64:
			a6->s6_addr[ 0] = nativi_config[0].dst_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].dst_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].dst_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].dst_prefix.s6_addr[3];
			a6->s6_addr[ 4] = nativi_config[0].dst_prefix.s6_addr[4];
			a6->s6_addr[ 5] = nativi_config[0].dst_prefix.s6_addr[5];
			a6->s6_addr[ 6] = nativi_config[0].dst_prefix.s6_addr[6];
			a6->s6_addr[ 7] = nativi_config[0].dst_prefix.s6_addr[7];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = ((uint8_t *)&a4)[0];
			a6->s6_addr[10] = ((uint8_t *)&a4)[1];
			a6->s6_addr[11] = ((uint8_t *)&a4)[2];
			a6->s6_addr[12] = ((uint8_t *)&a4)[3];
			a6->s6_addr[13] = 0;
			a6->s6_addr[14] = 0;
			a6->s6_addr[15] = 0;
			break;
		case 96:
			a6->s6_addr[ 0] = nativi_config[0].dst_prefix.s6_addr[0];
			a6->s6_addr[ 1] = nativi_config[0].dst_prefix.s6_addr[1];
			a6->s6_addr[ 2] = nativi_config[0].dst_prefix.s6_addr[2];
			a6->s6_addr[ 3] = nativi_config[0].dst_prefix.s6_addr[3];
			a6->s6_addr[ 4] = nativi_config[0].dst_prefix.s6_addr[4];
			a6->s6_addr[ 5] = nativi_config[0].dst_prefix.s6_addr[5];
			a6->s6_addr[ 6] = nativi_config[0].dst_prefix.s6_addr[6];
			a6->s6_addr[ 7] = nativi_config[0].dst_prefix.s6_addr[7];
			a6->s6_addr[ 8] = 0;
			a6->s6_addr[ 9] = nativi_config[0].dst_prefix.s6_addr[9];
			a6->s6_addr[10] = nativi_config[0].dst_prefix.s6_addr[10];
			a6->s6_addr[11] = nativi_config[0].dst_prefix.s6_addr[11];
			a6->s6_addr[12] = ((uint8_t *)&a4)[0];
			a6->s6_addr[13] = ((uint8_t *)&a4)[1];
			a6->s6_addr[14] = ((uint8_t *)&a4)[2];
			a6->s6_addr[15] = ((uint8_t *)&a4)[3];
			break;
		default:
			WARN_ON_ONCE(1);
		}
	}
}

